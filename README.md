# Projeto de Automação em Cypress com Estrutura Page Object

Este é um projeto de automação de testes de interface de usuário utilizando o framework Cypress e a estrutura Page Object.

# Pré-requisitos
Node.js instalado na máquina
npm instalado na máquina

# Instalação
Clone ou faça o download deste repositório
Acesse o diretório do projeto no terminal
Execute o comando npm install para instalar as dependências do projeto

# Execução de Testes
Acesse o diretório do projeto no terminal
Execute o comando npx cypress open para executar o cypress

# Estrutura de Pastas

. A pasta cypress/fixtures contém arquivos de fixtures utilizados nos testes
. A pasta cypress/integrations/page-objects contém a estrutura Page Object dos elementos da interface de usuário
. A pasta cypress/integrations contém os scripts de tes

# Estrutura Page Object
A estrutura Page Object é utilizada para manter o código de teste organizado e de fácil manutenção. Cada página da interface de usuário é representada por um objeto que contém as funções relacionadas aos elementos da página.

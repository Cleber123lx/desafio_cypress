var faker = require("faker-br");
var randomcpf = faker.br.cpf();
var randomEmail = faker.internet.email();


const LOGIN_PAGE_URL = "https://www.natura.com.br/cadastre-se?redirect=%2F";
const PERSONAL_DATA_URL = "https://www.natura.com.br/meus-dados/editar";




class LoginPage {
  visit() {
    cy.visit(LOGIN_PAGE_URL);
  }

  fillPersonalInformation(firstName, lastName, email, password, cpf, dateOfBirth, telephone) {
    cy.get('input[name="firstName"]').type(firstName);
    cy.get('input[name="lastName"]').type(lastName);
    cy.get('div input[name="email"]').type(email);
    cy.get("#password-field").type(password);
    cy.get("#confirmPassword-field").type(password);
    cy.get('input[name="cpf"]').type(cpf);
    cy.get("input[name='dateOfBirth']").type(dateOfBirth);
    cy.get(':nth-child(11) > .MuiFormControl-root > .MuiInputBase-root > .MuiInputBase-input').type(telephone);
  }

  acceptTermsAndConditions() {
    cy.get('#acceptedterms').click();
    cy.get('#onetrust-accept-btn-handler').click();
  }

  createAccount() {
    cy.get('.natds93 > .MuiButtonBase-root').dblclick();
  }

  goToPersonalData() {
    cy.wait(5000);
    cy.visit(PERSONAL_DATA_URL);
  }
}




const personalData = {
  firstName: "João",
  lastName: "Lucas",
  email: `deTES123${randomEmail}`,
  password: "Teste91823790¨&%&%#",
  cpf: randomcpf,
  dateOfBirth: '07/06/1981',
  telephone: "(11) 47649-5820"
  };

export { personalData, LoginPage };

const faker = require("faker-br");

const CPF = faker.br.cpf();
const EMAIL = faker.internet.email();

class LoginPage {
  visit() {
    cy.visit("https://www.aesop.com.br/cadastre-se");
  }

  fillForm() {
    cy.get("input[name='firstName']").type("Cleber");
    cy.get("input[name='lastName']").type("Araujo");
    cy.get("div input[name='email']").type(EMAIL);
    cy.get("#password-field").type("Teste91823790¨&%&%#");
    cy.get("#confirmPassword-field").type("Teste91823790¨&%&%#");
    cy.get("input[name='cpf']").type(CPF);
    cy.get(
      ":nth-child(3) > .MuiButtonBase-root > .MuiIconButton-label > .natds72"
    ).click();
    cy.get(
      ":nth-child(4) > :nth-child(1) > .MuiFormControl-root > .MuiInputBase-root > .MuiInputBase-input"
    ).type("07/06/1981");
    cy.get(
      ":nth-child(4) > :nth-child(2) > .MuiFormControl-root > .MuiInputBase-root > .MuiInputBase-input"
    ).type("(11) 47649-5820");
    cy.get("#receiveNewsLetter").click();
    cy.get("#infContOptIn").click();
    cy.get("#acceptedterms").click();
    cy.get('#onetrust-accept-btn-handler').click();
  }

  createAccount() {
    cy.get(".natds81 > .MuiButtonBase-root").dblclick();
  }

  goToPersonalData() {
    cy.wait(5000);
    cy.visit('https://www.aesop.com.br/meus-dados/editar');
  }
}

export default new LoginPage();

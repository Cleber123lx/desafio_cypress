/// <reference types="cypress" />
import { personalData, LoginPage } from "../../pages/loginPageNatura";

const loginPage = new LoginPage();

describe("criar conta Natura", () => {
  it("criar conta Natura", () => {
    loginPage.visit();
    loginPage.fillPersonalInformation(personalData.firstName, personalData.lastName, personalData.email, personalData.password, personalData.cpf, personalData.dateOfBirth, personalData.telephone);
    loginPage.acceptTermsAndConditions();
    loginPage.createAccount();
    loginPage.goToPersonalData();
  });
});

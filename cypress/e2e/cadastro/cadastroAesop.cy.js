/// <reference types="cypress" />
import LoginPage from "../../pages/loginPageAesop.js";

describe("criar conta aesop", () => {
  it("criar conta aesop", () => {
    LoginPage.visit();
    LoginPage.fillForm();
    LoginPage.createAccount();
    LoginPage.goToPersonalData();
  });
});
